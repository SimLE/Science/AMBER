#---------------------------------
#   MISSION SPECIFIC PARAMETERS
#---------------------------------

TEMP_1_SHALL = 13


#---------------------------------
#             PINS
#---------------------------------

#Blink LED
LED_PIN = 9

#Temperature sensors
TEMP_1_PIN = 17 

#Heating pads
PID_1_PIN = 19



#---------------------------------
#       CONSTANT PARAMETERS
#---------------------------------

# 1kHz main loop for µC
FREQ = 1000


#UART SHIT
UART_BAUDRATE = 115200
UART_TX = 0
UART_RX = 1

#Bytes corresponding to commands in uart
HEATPAD_START = b'S'
HEATPAD_STOP = b'E'

#HOW many seconds to wait between temperature measurements
TEMP_1_FREQ = 1

#Webserver shit
SSID = "AMBER_PAYLOAD"
PASSWORD = "12345678"

HTML = """<!DOCTYPE html>
    <html>
        <head> <title>SUPER DUPER AMBER WEB INTERFACE</title> </head>
        <body> <h1>Shitcode is real</h1>
            <p>%s</p>
        </body>
    </html>
    """

#This is the order of the data is presented in the txt file
FORMAT_STR = ",".join(
    (
        #"{restarts}",
        "{temp_cool:.3},{temp_hot:.3},{temp_ambi:.3}",
        "{temp_internal:.3}",
        "{acc_x:.3},{acc_y:.3},{acc_z:.3}",
        "{gyro_x:.3},{gyro_y:.3},{gyro_z:.3}",
        "{mag_x:.3},{mag_y:.3},{mag_z:.3}",
        #",{date}",
        ",{time}",
        #",{datetime}",
        "{pid_output}"
    ))


PID_1_K = {
    "Kp": -1.4,
    "Ki": -0.07,
    "Kd": -5,
}

PID_DT = TEMP_1_FREQ

#---------------------------------
#     HARDWARE CONFIGURATION
#---------------------------------

# add more addreses here
# label them as A, B, C...
ADDR_A = {
    "hot":bytearray(b'(\xa9\xac\x8a\r\x00\x00\x14'),
    "cool": bytearray(b'(\x90\x15\x8b\r\x00\x00\\'),
    "ambi": bytearray(b'(\xc0\xa9\x8a\r\x00\x00\x93')
}


#Used for internal temp od pi pico
CONVERSION_FACTOR = 3.3 / 65535





