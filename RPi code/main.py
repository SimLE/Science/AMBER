"""
    AMBER Raspberry Pi Pico main program
    Authors: J.L., N.F.S, M.J.
    Date: 09.06.2023
    version: 1.0
"""

from machine import Pin, Timer, ADC, I2C, RTC, PWM, UART
from amber_sd import SD
from amber_temp import Temp
from amber_imu import MinIMU
from amber_pid import PID
from amber_rtc import DS3231
from amber_webserver import *
import amber_config as ac
import time

def dt_to_filename_str(datetime):
    # TODO: use this, test it!
    # return "{0}{1:02d}{2:02d}{3:02d}{4:02d}{5:02d}{6:02d}".format(*datetime)
    return (
        str(datetime[0])
        + "{:02d}".format(datetime[1])
        + "{:02d}".format(datetime[2])
        + "{:02d}".format(datetime[4])
        + "{:02d}".format(datetime[5])
        + "{:02d}".format(datetime[6])
    )

def time_to_str(datetime):
    # TODO: use this, test it!
    # return "{0:02d}:{1:02d}:{2:02d}".format(*datetime[3:6])
    return (
        "{:02d}".format(datetime[4])
        + ":"
        + "{:02d}".format(datetime[5])
        + ":"
        + "{:02d}".format(datetime[6])
    )

def date_to_str(datetime):
    # TODO: use this, test it!
    # return "{2:02d}.{1:02d}.{2}".format(*datetime[0:3])
    return (
        "{:02d}".format(datetime[2])
        + "."
        + "{:02d}".format(datetime[1])
        + "."
        + str(datetime[0])
    )

def datetime_to_str(datetime):
    # TODO: use this, test it!
    # return "{0}-{1:02d}-{2:02d}T{4:02d}:{5:02d}:{6:02d}".format(*datetime)
    return (
        str(datetime[0])
        + "-"
        + "{:02d}".format(datetime[1])
        + "-"
        + "{:02d}".format(datetime[2])
        + "T"
        + "{:02d}".format(datetime[4])
        + ":"
        + "{:02d}".format(datetime[5])
        + ":"
        + "{:02d}".format(datetime[6])
    )


# FLAGS
ENABLE_PRINT = False  
READ_INTERNAL_TEMP = True
READ_IMU = True
READ_RTC = False
ENABLE_TEMP_1 = True
ENABLE_PID_1 = True

# Here you select which sensors go to which 1 wire line
ADDR_1 = ac.ADDR_A

#Init UART
uart = UART(0, baudrate=ac.UART_BAUDRATE, tx=Pin(ac.UART_TX), rx=Pin(ac.UART_RX))
uart.init(bits=8, parity=None, stop=2)

# Init Temp
temp_1 = Temp(pin=ac.TEMP_1_PIN)

# Internal Temp of RPI Pico
internal_temp_raw = ADC(4)

# Init RTC
rtc = DS3231(freq=100000)
datetime = rtc.DateTime()
datetime_str = dt_to_filename_str(datetime)


# Init sd Card
sd = SD(filename= datetime_str + "_AMBER_DATA.txt")
restarts = sd.restarts

# Init IMU
imu = MinIMU(freq=1000000)

# Init PID
pid_1 = PID(pin=ac.PID_1_PIN)
pid_1_out = 0

#init LED
led = Pin(ac.LED_PIN, Pin.OUT)

#init Heating pad
heat_pad_1 = Pin(ac.PID_1_PIN, Pin.OUT)

#Init webserver
socket,wlan = init_webserver(ac.SSID, ac.PASSWORD)

def read_internal_temp():
    reading = internal_temp_raw.read_u16() * ac.CONVERSION_FACTOR
    return 27 - (reading - 0.706) / 0.001721

def send_frame(**data):
    row = ac.FORMAT_STR.format(**data)
    sd.write(row)
    if ENABLE_PRINT:
        print("/*", row, "*/", sep="")

def handle_uart():
    #disable the PID if you plan on using this, or fix the code to disable it automatically
    if uart.any(): 
            data = uart.read()
            if data == ac.HEATPAD_START:
                heat_pad_1.value(1)

            if data == ac.HEATPAD_STOP:
                heat_pad_1.value(0)
            
def loop(temp_1_timer):
    
    handle_uart()
        
    # Toggle LED
    led.value(not led.value())

    # Read internal temp
    if READ_INTERNAL_TEMP:
        internal_temp = read_internal_temp()
    else:
         internal_temp = 0

    # Read IMU
    if READ_IMU:
        acc = imu.readAccelerometer()
        gyro = imu.readGyro()
        mag = imu.readMagnetometer()
    else:
        acc = 0
        gyro = 0 
        mag = 0

    # Read RTC
    if READ_RTC:
        datetime = list(rtc.DateTime())
        datetime_str = datetime_to_str(datetime)
        date_str = date_to_str(datetime)
        time_str = time_to_str(datetime)
    
    if ENABLE_TEMP_1 and time.time() - temp_1_timer > ac.TEMP_1_FREQ:
        temp_1_timer = time.time()
        temp_1_cool = temp_1.get_temp(ADDR_1["cool"])
        temp_1_hot = temp_1.get_temp(ADDR_1["hot"])
        temp_1_ambi = temp_1.get_temp(ADDR_1["ambi"])
              
        # PID Control
        if ENABLE_PID_1:
            pid_1_out = pid_1.pid_control(
                **ac.PID_1_K, temp_shall=ac.TEMP_1_SHALL, temp_is=temp_1_cool, delta_t=ac.PID_DT,
            )
        else:
            pid_1_out = 0
    else:
        temp_1_cool = 0
        temp_1_hot = 0
        temp_1_ambi = 0
        pid_1_out = 0
        
            
    send_frame(
        temp_cool=temp_1_cool,
        temp_hot=temp_1_hot,
        temp_ambi=temp_1_ambi,
        temp_internal=internal_temp,
        acc_x=acc[0],
        acc_y=acc[1],
        acc_z=acc[2],
        gyro_x=gyro[0],
        gyro_y=gyro[1],
        gyro_z=gyro[2],
        mag_x=mag[0],
        mag_y=mag[1],
        mag_z=mag[2],
        time=time.time(),
        pid_output=pid_1_out
    )
    return temp_1_timer # this is a very weird way to do this timer, but for some reason it just won't accept a  temp_1_timer as a global variable


led.value(not led.value())
time.sleep(0.5)
led.value(not led.value())

listen_for_start(socket,wlan,led)
del socket
del wlan

temp_timer = 0
while True:
    temp_timer = loop(temp_timer)



# Init Timer for main loop
#timer = Timer()
# Program entrypoint
#timer.init(freq=ac.FREQ, mode=Timer.PERIODIC, callback=loop)

