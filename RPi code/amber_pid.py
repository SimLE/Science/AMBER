from machine import Pin, PWM


class PID:
    def __init__(self, pin=18, pwm_freq=100):

        self.pwm = PWM(Pin(pin, Pin.OUT))
        self.pwm.freq(pwm_freq)
        self.pwm.duty_u16(0)

        self.p_out = 0
        self.i_out = 0
        self.d_out = 0
        self.e_prev = 0

        # Max Value because of unsigned integer 16 bits
        self.max_out = 2 ** 16 - 1
        # Conditioning value for Output
        self.cond = self.max_out / 26.81

    def set_pwm(self, duty):
        self.pwm.duty_u16(duty)

    def pid_control(self, Kp, Ki, Kd, temp_shall, temp_is, delta_t):

        e = temp_shall - temp_is

        self.p_out = Kp * e

        self.i_out = self.i_out + Ki * e * delta_t

        self.d_out = Kd * (e - self.e_prev) / delta_t
        self.e_prev = e

        pid_out = (self.p_out + self.i_out + self.d_out) * self.cond

        # Limiter
        if pid_out < 0.0:
            pid_out = 0
        elif pid_out > self.max_out:
            pid_out = self.max_out

        # Set PWM
        self.pwm.duty_u16(int(pid_out))

        return pid_out
