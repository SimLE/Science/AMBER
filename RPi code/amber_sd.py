from machine import Pin, SPI
import sdcard
import os

class SD():
    
    def __init__(self, spi_id=1, sck=10, mosi=11, miso=12, cs=13, baudrate=40000000, filename="_AMBER_DATA.txt"):
        
        self.write_counter = 0
        self.restarts_filename = "restarts.txt"
        self.data_filename = filename
        self.restarts = 0
        
        #global restarts, data_filename
        # Initialize the SD card
        self.spi=SPI(spi_id,baudrate=baudrate,sck=Pin(sck),mosi=Pin(mosi),miso=Pin(miso))
        self.sd=sdcard.SDCard(self.spi,Pin(cs))
        # Create a instance of MicroPython Unix-like Virtual File System (VFS),
        self.vfs=os.VfsFat(self.sd) 
        # Mount the SD card
        os.mount(self.sd,'/sd')
        # Set number of restarts into sdcard
        updated_restarts = 0
        try:
            # Read _AmberData.txt
            with open("/sd/"+self.restarts_filename, "r") as file:
                already_restarted = file.readline()
            
            # Update restarts
            with open("/sd/"+self.restarts_filename, "w") as file:
                updated_restarts = int(already_restarted) + 1
                file.write(str(updated_restarts))
                
        # If there was no file so far
        except:
            with open("/sd/"+self.restarts_filename, "w") as file:
                file.write('1')
        
                
        self.restarts = updated_restarts
        
        self.data_file = open("/sd/"+self.data_filename, 'a')
        headline = "Temp_cool,Temp_hot,Temp_ambi,Temp_internal,acc_x,acc_y,acc,z,gyro_x,gyro_y,gyro_z,mag_x,mag_y,mag_z,Time,PID_1_OUT"
        self.data_file.write(headline)
        self.data_file.flush()
        
        # Create filename
        
        
            
    def listdir(self):
        # Debug print SD card directory and files
        print(os.listdir('/sd'))
        
    def write(self,data):
        
        self.data_file.write("\r\n"+data)
        
        if self.write_counter == 10:
            self.data_file.flush()
            self.write_counter = 0
            
        self.write_counter += 1
        

