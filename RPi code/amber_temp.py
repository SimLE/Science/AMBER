import machine, onewire, ds18x20, time


class Temp:
    def __init__(self, pin=16):

        self.pin = pin

        # Set GPIO 17 as data pin for temp sensors
        self.ds_pin = machine.Pin(self.pin)
        self.ds_sensor = ds18x20.DS18X20(onewire.OneWire(self.ds_pin))

    def scan(self):
        return self.ds_sensor.scan()

    def get_temp(self, rom):
        self.ds_sensor.convert_temp()
        return self.ds_sensor.read_temp(rom)
